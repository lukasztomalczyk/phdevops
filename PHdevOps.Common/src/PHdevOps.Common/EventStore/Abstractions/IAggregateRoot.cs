﻿using System;
using System.Collections.Generic;

namespace PHdevOps.Common.EventStore.Abstractions
{
    public interface IAggregateRoot
    {
        List<object> GetEvents();

        void ClearEvents();

        void Apply(object e);

        Guid Id { get; }

        int Version { get; }
    }
}