﻿using System;
using System.Text;
using Newtonsoft.Json;
using PHdevOps.Common.EventBus.Abstractions;
using PHdevOps.Common.EventBus.Events;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace PHdevOps.Common.EventBusRabbitMQ
{
    public class EventBusRabbitMq : IEventBus
    {
        public EventBusRabbitMq()
        {
            var factory = new ConnectionFactory() {HostName = _connectionString, UserName = "user", Password = "password"};
            _connection  = factory.CreateConnection();
        }
        
        private string _connectionString = "172.17.0.2";
        private string _brokerName = "Rabbit";
        private IConnection _connection;

        public void Publish(IntegrationEvent @event)
        {
            var eventName = @event.GetType().Name;

            using (var channel = _connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _brokerName,
                    type: "direct");
                string message = JsonConvert.SerializeObject(@event);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: _brokerName,
                    routingKey: eventName,
                    basicProperties: null,
                    body: body);
            }
        }

        public void Subscribe<T, TH>() where T : IntegrationEvent where TH : IIntegrationEventHandler<T>
        {

                using (var channel = _connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: _brokerName,
                        type: "direct");
                    
                    var queueName = channel.QueueDeclare().QueueName;
                    
                    channel.QueueBind(queue: queueName,
                        exchange: _brokerName,
                        routingKey: "name");
                    
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine(" [x] {0}", message);
                    };
                }
        }

        public void Unsubscribe<T, TH>() where T : IntegrationEvent where TH : IIntegrationEventHandler<T>
        {
            throw new System.NotImplementedException();
        }
    }
}