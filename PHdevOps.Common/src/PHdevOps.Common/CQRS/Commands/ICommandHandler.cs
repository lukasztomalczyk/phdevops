﻿using System.Threading.Tasks;

namespace PHdevOps.Common.CQRS.Commands
{
    public interface ICommandHandler<in TCommand>
    {
        Task SendAsync(TCommand command);
    }
}