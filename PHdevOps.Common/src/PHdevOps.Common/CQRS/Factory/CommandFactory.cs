﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using PHdevOps.Common.CQRS.Commands;

namespace PHdevOps.Common.CQRS.Factory
{
    public class CommandFactory : ICommandFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public CommandFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        
        public ICommandHandler<TCommand> Create<TCommand>()
        {
            var list = new List<object>();
            foreach (var handler in _serviceProvider.GetServices<ICommandHandler<TCommand>>())
            {
            list.Add(handler);
            }
            
            return _serviceProvider.GetService<ICommandHandler<TCommand>>();
        }
    }
}