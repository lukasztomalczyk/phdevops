﻿using PHdevOps.Common.CQRS.Commands;

namespace PHdevOps.Common.CQRS.Factory
{
    public interface ICommandFactory
    {
        ICommandHandler<TCommand> Create<TCommand>();
    }
}