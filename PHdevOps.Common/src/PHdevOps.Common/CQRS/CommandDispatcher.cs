﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PHdevOps.Common.CQRS.Commands;
using PHdevOps.Common.CQRS.Factory;

namespace PHdevOps.Common.CQRS
{
    public class CommandDispatcher : ICommandDispatcher
    {
        private readonly ICommandFactory _commandFactory;


        public CommandDispatcher(ICommandFactory commandFactory)
        {
            _commandFactory = commandFactory;
        }

        public async Task DispatchAsync<TCommand>(TCommand command)
        {
            if(command == null)
                throw new ArgumentNullException();

            var result = _commandFactory.Create<TCommand>();

            await result.SendAsync(command);
        }
    }
}