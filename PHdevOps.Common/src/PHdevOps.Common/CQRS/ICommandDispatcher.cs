﻿using System.Threading.Tasks;

namespace PHdevOps.Common.CQRS
{
    public interface ICommandDispatcher
    {
        Task DispatchAsync<TCommand>(TCommand command);
    }
}