﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PHdevOps.Common.CQRS;
using PHdevOps.Common.CQRS.Commands;
using PHdevOps.Common.EventBus.Abstractions;
using PHdevOps.Common.EventBusRabbitMQ;
using PHdevOps.Orders.CQRS.Commands;
using PHdevOps.Orders.Domain;

namespace PHdevOps.Orders.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public OrdersController(ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
        }

        [HttpGet]
        public void Poc()
        {
            _commandDispatcher.DispatchAsync(new CreateOrderCommand());
        }

        [HttpGet]
        public void Create(CreateOrderCommand command)
        {
            _commandDispatcher.DispatchAsync(command);
        }

    }
}