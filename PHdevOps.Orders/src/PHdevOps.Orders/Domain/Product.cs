﻿using PHdevOps.Common.EventBus.Events;

namespace PHdevOps.Orders.Domain
{
    public class Product : IntegrationEvent
    {
        public string  Name { get; set; }
    }
}