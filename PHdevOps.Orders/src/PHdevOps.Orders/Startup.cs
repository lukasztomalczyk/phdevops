﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PHdevOps.Common.CQRS;
using PHdevOps.Common.CQRS.Factory;
using PHdevOps.Common.EventBus.Abstractions;
using PHdevOps.Common.EventBusRabbitMQ;
using PHdevOps.Orders.Extensions;

namespace PHdevOps.Orders
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Potrzebne Ci asemby, zestaw meta danych poejktu w którym ma skanować i zarejestrować abstrakcje
            var assemblies = Assembly.GetExecutingAssembly();
            services.AddSingleton<IEventBus, EventBusRabbitMq>();

            //Zamknij to w jednej klasie
            services.AddCommandHandlers(assemblies);
            services.AddScoped<ICommandFactory, CommandFactory>();
            services.AddScoped<ICommandDispatcher, CommandDispatcher>();
            services.AddEventHandlers(assemblies);
            //
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                loggerFactory.AddDebug();
                loggerFactory.AddConsole();
                app.UseDeveloperExceptionPage();
            }
            //TODO:  Zbędne
            else
            {
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseMvc();

            //TODO: reafator
            //ConfigureEventBus(app);
        }
        
        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

        //    eventBus.Subscribe<ProductPriceChangedEvent, ProductPriceChangedEventHandler>();
        }
    }
}
