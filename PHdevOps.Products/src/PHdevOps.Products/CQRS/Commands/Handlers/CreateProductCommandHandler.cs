﻿using System.Threading.Tasks;
using PHdevOps.Common.CQRS.Commands;
using PHdevOps.Common.EventBus.Abstractions;
using PHdevOps.Products.IntegrationEvents.Events;

namespace PHdevOps.Products.CQRS.Commands.Handlers
{
    public class CreateProductCommandHandler : ICommandHandler<CreateProductCommand>
    {
        private readonly IEventBus _eventBus;

        public CreateProductCommandHandler(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }
        public Task SendAsync(CreateProductCommand command)
        {
            var @event = new CreatedProductEvent();
            _eventBus.Publish(@event);
            
            return Task.CompletedTask;
        }
    }
}