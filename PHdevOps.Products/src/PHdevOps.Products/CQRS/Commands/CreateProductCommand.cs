﻿using PHdevOps.Common.CQRS.Commands;
using PHdevOps.Products.Domain;

namespace PHdevOps.Products.CQRS.Commands
{
    public class CreateProductCommand : ICommand
    {
        public string Name { get; set; }
        public CategoryEnum Category { get; set; }
        public float Price { get; set; }
    }
}