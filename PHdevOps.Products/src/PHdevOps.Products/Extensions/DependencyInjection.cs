﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using PHdevOps.Common.CQRS.Commands;
using PHdevOps.Common.EventBus.Abstractions;

namespace PHdevOps.Products.Extensions
{
    public static class DependencyInjection
    {
        public static void AddCommandHandlers(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.Scan(scan => scan.FromAssemblies(assemblies)
                .AddClasses(classes => classes.AssignableTo(typeof(ICommandHandler<>)))
                .AsImplementedInterfaces()
                .WithScopedLifetime());
        }
        
        public static void AddEventHandlers(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.Scan(scan => scan.FromAssemblies(assemblies)
                .AddClasses(classess => classess.AssignableTo(typeof(IIntegrationEventHandler<>)))
                .AsImplementedInterfaces()
                .WithTransientLifetime());
        }
    }
}