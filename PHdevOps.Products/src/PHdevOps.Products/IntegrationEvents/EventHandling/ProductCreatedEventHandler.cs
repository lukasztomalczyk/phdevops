﻿using System;
using System.Threading.Tasks;
using PHdevOps.Common.EventBus.Abstractions;
using PHdevOps.Products.IntegrationEvents.Events;

namespace PHdevOps.Products.IntegrationEvents.EventHandling
{
    public class ProductCreatedEventHandler : IIntegrationEventHandler<CreatedProductEvent>
    {
        public Task Handle(CreatedProductEvent @event)
        {
            Console.WriteLine("Product event handler");
            return Task.CompletedTask;
            
        }
    }
}