﻿namespace PHdevOps.Products.Domain
{
    public enum CategoryEnum
    {
        Elektronika = 0,
        AGD = 1,
        Inne = 2
    }
}