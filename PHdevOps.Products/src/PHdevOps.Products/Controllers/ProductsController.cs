﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PHdevOps.Common.CQRS;
using PHdevOps.Common.EventBus.Abstractions;
using PHdevOps.Products.CQRS.Commands;


namespace PHdevOps.Products.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public ProductsController(ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
        }
        
        [HttpPost]
        public async Task Create([FromBody] CreateProductCommand command)
        {
            await _commandDispatcher.DispatchAsync(command);
        }

        [HttpGet]
        public string Test()
        {
            return "test";
        }
    }

}
